DELETE FROM bids;
DELETE FROM products;
DELETE FROM users;

ALTER TABLE users AUTO_INCREMENT = 1;
ALTER TABLE products AUTO_INCREMENT = 1;
ALTER TABLE bids AUTO_INCREMENT = 1;

/*************************************************************
 **						Populate T_User 					**
 *************************************************************/

INSERT INTO users VALUES
	(default, "fumane_t@epita.fr", "Thibaut", "Fumaneri", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "digest_e@epita.fr", "Edgardo", "Di gesto", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "floren_d@epita.fr", "Dat", "Florentin", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "herrba_k@epita.fr", "Kevin", "Herrbach", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "john.doe@gmail.com", "John", "Doe", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "max.cena@gmail.com", "Max", "Cena", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "jean.didier@gmail.com", "Jean", "Didier", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "eric.simon@gmail.com", "Eric", "Simon", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "arfi.jl@gmail.com", "Jean-luc", "Arfi", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default),
	(default, "simon.cedric@epita.fr", "Simon", "Cedric", "$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy", default);


/*************************************************************
 **						Populate Products 					**
 *************************************************************/

	/* ----- USERID fumane_t@epita.fr ----- */
	SET @userid	= 1;

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 7 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 7 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Chaussures Luxe", "Chaussures très luxueuses pas cher !", "Chaussures", @StartDate, @EndDate, 101.23, 105, 0);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 2 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 21 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Raquette de badminton", "Bon état, quasi neuve", "Sport", @StartDate, @EndDate, 65, 67.50, 0);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 4 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 12 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Rangement maquillage", "2 tiroirs, fonctionnel, bon état", "Salle de bain", @StartDate, @EndDate, 10, 12, 0);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL -10 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 5 DAY);
	INSERT INTO products VALUES
	(default, @userid, "guitare elypse noire", "Elle a 1 ans, fournis avec les cordes", "Musique", @StartDate, @EndDate, 122, 128, 1);


	SET @StartDate = DATE_ADD(NOW(),INTERVAL 7 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 13 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Tondeuse électrique", "Garantie encore 2 ans, bon état, servis avec emballage", "Bricolage", @StartDate, @EndDate, 50, 0, 0);




	/* ----- USERID digest_e@epita.fr ----- */
	SET @userid	= 2;

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 8 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 4 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Livre génial", "Très agréable à lire", "Livres", @StartDate, @EndDate, 12.50, 13, 0);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 10 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 20 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Clavier sans fil", "Clavier avec bluetooth, peu servis, beau design", "Informatique", @StartDate, @EndDate, 44, 67.12, 0);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL -15 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 6 DAY);
	INSERT INTO products VALUES
	(default, @userid, "DVD James bond", "Meilleur dvd, à acheter sans réfléchir !", "DVD, cinéma", @StartDate, @EndDate, 10, 12, 1);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 7 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 3 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Pc portable Asus", "Entierement révisé avant de le mettre en vente", "Informatique", @StartDate, @EndDate, 380, 0, 0);


	SET @StartDate = DATE_ADD(NOW(),INTERVAL 2 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 13 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Arbre à chat", "9 couleurs, Avec hamacs, plate-forme, niches", "Animalerie", @StartDate, @EndDate, 63.95, 0, 0);




	/* ----- USERID floren_d@epita.fr ----- */
	SET @userid	= 3;

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 7 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 3 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Lot De 30 Capsules Nespresso Volluto", "Capsules de qualité", "Électroménager", @StartDate, @EndDate, 8.99, 9, 0);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL 20 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 3 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Fer à souder sans fil 8 W Silverline 220772", "Entierement révisé avant de le mettre en vente", "Bricolage", @StartDate, @EndDate, 380, 415, 0);

	SET @StartDate = DATE_ADD(NOW(),INTERVAL -20 DAY);
	SET @EndDate = DATE_ADD(@StartDate, INTERVAL 3 DAY);
	INSERT INTO products VALUES
	(default, @userid, "Chocolat noir", "Vente d'un stock de chocolat noir à bas prix", "Alimentation", @StartDate, @EndDate, 15, 20, 1);


/*************************************************************
**						Populate Bids  				   	    **
*************************************************************/

SET @StartDate = DATE_ADD(NOW(),INTERVAL 11 DAY);
INSERT INTO bids VALUES (default, 3, 1, @StartDate, 105, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 8 DAY);
INSERT INTO bids VALUES (default, 4, 1, @StartDate, 103.30, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 9 DAY);
INSERT INTO bids VALUES (default, 5, 1, @StartDate, 103.70, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 10 DAY);
INSERT INTO bids VALUES (default, 8, 1, @StartDate, 104.55, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 18 DAY);
INSERT INTO bids VALUES (default, 3, 2, @StartDate, 67.50, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 3 DAY);
INSERT INTO bids VALUES (default, 6, 2, @StartDate, 65.20, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 5 DAY);
INSERT INTO bids VALUES (default, 8, 2, @StartDate, 66, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 14 DAY);
INSERT INTO bids VALUES (default, 7, 2, @StartDate, 67.25, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 14 DAY);
INSERT INTO bids VALUES (default, 9, 3, @StartDate, 12, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 7 DAY);
INSERT INTO bids VALUES (default, 7, 3, @StartDate, 11, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL -7 DAY);
INSERT INTO bids VALUES (default, 3, 4, @StartDate, 128, 1);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 11 DAY);
INSERT INTO bids VALUES (default, 3, 6, @StartDate, 13, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 9 DAY);
INSERT INTO bids VALUES (default, 5, 6, @StartDate, 12.55, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 10 DAY);
INSERT INTO bids VALUES (default, 1, 6, @StartDate, 12.80, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 20 DAY);
INSERT INTO bids VALUES (default, 9, 7, @StartDate, 67.12, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 11 DAY);
INSERT INTO bids VALUES (default, 4, 7, @StartDate, 44.20, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 12 DAY);
INSERT INTO bids VALUES (default, 5, 7, @StartDate, 45, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 15 DAY);
INSERT INTO bids VALUES (default, 6, 7, @StartDate, 52, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 19 DAY);
INSERT INTO bids VALUES (default, 10, 7, @StartDate, 64.50, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL -8 DAY);
INSERT INTO bids VALUES (default, 3, 8, @StartDate, 12, 1);

SET @StartDate = DATE_ADD(NOW(),INTERVAL -14 DAY);
INSERT INTO bids VALUES (default, 5, 8, @StartDate, 10.50, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL -10 DAY);
INSERT INTO bids VALUES (default, 6, 8, @StartDate, 11, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 8 DAY);
INSERT INTO bids VALUES (default, 7, 11, @StartDate, 9, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 23 DAY);
INSERT INTO bids VALUES (default, 1, 12, @StartDate, 415, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 21 DAY);
INSERT INTO bids VALUES (default, 2, 12, @StartDate, 401.44, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL 20 DAY);
INSERT INTO bids VALUES (default, 5, 12, @StartDate, 395, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL -18 DAY);
INSERT INTO bids VALUES (default, 1, 13, @StartDate, 16, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL -19 DAY);
INSERT INTO bids VALUES (default, 1, 13, @StartDate, 15.50, 0);

SET @StartDate = DATE_ADD(NOW(),INTERVAL -19 DAY);
INSERT INTO bids VALUES (default, 2, 13, @StartDate, 15.75, 0);
