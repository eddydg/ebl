<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProductController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\I18n\Date;

/**
 * App\Controller\ProductController Test Case
 */
class ProductControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.products',
        'app.users',
        'app.bids'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->Product = TableRegistry::get('Product');
    }

    public function testFindFirstProduct()
    {
        $result = $this->Product->get(1);
        $expected = [
                'id' => 1,
                'user_id' => 1,
                'title' => 'Jouet pour chien',
                'description' => 'Pour un max de défoulement',
                'category' => 'Animalerie',
                'date_start' => '2016-07-02 22:29:00',
                'date_end' => '2016-07-09 22:29:00',
                'price_start' => 1.5,
                'price_instant' => 1.5,
                'is_over' => false
            ];
        $this->assertEquals($expected['id'], $result['id']);
        $this->assertEquals($expected['title'], $result['title']);
    }

    public function testCategory()
    {
        $query = $this->Product->find('all');
        $result = $query->hydrate(false)->toArray();
        $expected = [
            'Animalerie',
            'Bricolage',
            'Chaussures',
            'DVD, cinéma',
            'Électroménager',
            'Informatique',
            'Livres',
            'Musique',
            'Salle de bain',
            'Sport'
        ];
        for ($i=0; $i < count($result) - 1; $i++)
        {
            $this->assertEquals($expected[$i], $result[$i]['category']);
        }
    }

    public function testFindSameProductsCount()
    {
        $query = $this->Product->find('all');
        $result = count($query->hydrate(false)->toArray());
        $expected = 10;
        $this->assertEquals($expected, $result);
    }

    public function testDetailUnauthenticated()
    {
        $this->get('/product/1');

        $this->assertResponseSuccess();
        $this->assertResponseCode(302);
        $this->assertRedirect([
            'controller' => 'User',
            'action'    => 'login'
        ]);
    }

    public function testDetailAuthenticated()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'digest_e@epita.fr',
                ]
            ]
        ]);

        $this->get('/product/1');

        $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $query = $this->Product->find('all');
        $beforeAdd = count($query->hydrate(false)->toArray());

        $date_end = date("Y-m-d H:i:s", strtotime('2016-07-12 23:53:57'));
        $date_start = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
        $datetime_end = new \DateTime($date_end,  new \DateTimeZone('Europe/Paris'));


        $product = $this->Product->newEntity();
        $product->title = "title";
        $product->user_id = 1;
        $product->description = "description";
        $product->category = "Animalerie";
        $product->date_start = $date_start->format('Y-m-d H:i:s');
        $product->date_end = $datetime_end->format('Y-m-d H:i:s');
        $product->price_start = 100;
        $product->price_instant = 100;
        $product->is_over = false;

        $this->Product->save($product);
        $afterAdd = count($query->hydrate(false)->toArray());

        $this->assertEquals($beforeAdd + 1, $afterAdd);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $product = $this->Product->get(2);
        $product->price_start = 999;
        $this->Product->save($product);
        $productstr = $this->Product->get(2);
        $this->assertEquals(999, $productstr->price_start);
    }

    /**
     * Test delete method
     *
     * @return void
     */
     /*
    public function testDelete()
    {
        $query = $this->Product->find('all');
        $expected = count($query->hydrate(false)->toArray());

        $product = $this->Product->get(1);
        $this->Product->delete($product);

        $query = $this->Product->find('all');
        $result = count($query->hydrate(false)->toArray());
        $this->assertEquals($expected - 1, $result);

    }*/

    public function testSearchQuery()
    {
        $query = 'asus';
        $results = array();
        if (strlen($query) > 0)
        {
            $query = htmlspecialchars($query);
            $query_words = explode(' ', $query);
            foreach ($this->Product->find('all') as $product)
            {
                $exp = '/'.implode('|', array_map('preg_quote', $query_words)).('/i');
                if (preg_match($exp, $product['title']))
                    array_push($results, $product);
            }
        }
        $expected = 'Ecran Asus';
        $result = $results[0]['title'];

        $this->assertEquals($expected, $result);
    }

    /**
     * Auth methods
     */

    public function testAddProductUnauthenticated()
    {
        $this->get('/product/add');

        $this->assertResponseSuccess();
        $this->assertResponseCode(302);
        $this->assertRedirect([
            'controller' => 'User',
            'action'    => 'login'
        ]);
    }

    public function testAddProductAuthenticated()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'email' => 'digest_e@epita.fr',
                ]
            ]
        ]);

        $this->get('/product/add');

        $this->assertResponseOk();
    }
}
