<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UserController;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UserController Test Case
 */
class UserControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.bids',
        'app.products'
    ];


    public function testIndexUnauthenticatedFails()
    {
        $this->get('/user');

        $this->assertResponseSuccess();
        $this->assertResponseCode(302);
        $this->assertRedirect([
            'controller' => 'User',
            'action'    => 'login'
        ]);
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    public function testRegisterIncomplete()
    {
        $testEmail = 'test@test.fr';
        $this->post('/user/add', [
            'email'     => $testEmail,
            'firstname' => 'test',
            'password'  => 'test'
        ]);

        $this->assertResponseSuccess();
        $this->assertResponseContains('Impossible de créer le compte');
    }

    public function testRegisterExistingEmail()
    {
        $testEmail = 'john.doe@gmail.com';
        $this->post('/user/add', [
            'email'     => $testEmail,
            'firstname' => 'test',
            'lastname' => 'test',
            'password'  => 'test'
        ]);

        $this->assertResponseSuccess();
        $this->assertResponseContains('This value is already in use');
    }

    public function testRegister()
    {
        $testEmail = 'test@test.fr';
        $this->post('/user/add', [
            'email'     => $testEmail,
            'firstname' => 'test',
            'lastname' => 'test',
            'password'  => 'test'
        ]);

        $this->assertRedirect(['controller' => 'Pages', 'action' => 'index']);

        $this->User = TableRegistry::get('User');
        $query = $this->User
            ->find()
            ->where(['email' => $testEmail])
            ->first();

        $this->assertNotNull($query);
    }

    public function testLoginIncomplete()
    {
        $this->post('/user/login', [
            'email'     => 'test@test.fr'
        ]);

        $this->assertResponseSuccess();
        $this->assertResponseContains('Email ou mot de passe invalides');
    }

    public function testLoginFail()
    {
        $this->post('/user/login', [
            'email'     => 'test@test.fr',
            'password'  => 'test'
        ]);

        $this->assertResponseSuccess();
        $this->assertResponseContains('Email ou mot de passe invalides');
    }

    public function testLogin()
    {
        $this->post('/user/login', [
            'email'     => 'john.doe@gmail.com',
            'password'  => 'test'
        ]);

        $this->assertResponseCode(302);
        $this->assertRedirect(['controller' => 'Pages', 'action' => 'index']);
        $this->assertSession('john.doe@gmail.com', 'Auth.User.email');
    }
}
