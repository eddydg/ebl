<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BidTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BidTable Test Case
 */
class BidTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BidTable
     */
    public $Bid;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bids',
        'app.users',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bid') ? [] : ['className' => 'App\Model\Table\BidTable'];
        $this->Bid = TableRegistry::get('Bid', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bid);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
