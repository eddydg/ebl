<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture
{

    public $import = ['model' => 'user', 'records' => true];

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'email' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'firstname' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'lastname' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'password' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'money' => ['type' => 'float', 'null' => true, 'default' => 100.00, 'comment' => '', 'precision' => 10, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */

    public $records = [
        [
            'email'     => 'fumane_t@epita.fr',
            'firstname' => 'Thibaut',
            'lastname'  => 'Fumaneri',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'digest_e@epita.fr',
            'firstname' => 'Edgardo',
            'lastname'  => 'Di gesto',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'floren_d@epita.fr',
            'firstname' => 'Dat',
            'lastname'  => 'Florentin',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'herrba_k@epita.fr',
            'firstname' => 'Kevin',
            'lastname'  => 'Herrbach',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'john.doe@gmail.com',
            'firstname' => 'John',
            'lastname'  => 'Doe',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'max.cena@gmail.com',
            'firstname' => 'Max',
            'lastname'  => 'Cena',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'jean.didier@gmail.com',
            'firstname' => 'Jean',
            'lastname'  => 'Didier',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'eric.simon@gmail.com',
            'firstname' => 'Eric',
            'lastname'  => 'Simon',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'arfi.jl@gmail.com',
            'firstname' => 'Jean-luc',
            'lastname'  => 'Arfi',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ],
        [
            'email'     => 'simon.cedric@epita.fr',
            'firstname' => 'Simon',
            'lastname'  => 'Cedric',
            'password'  => '$2y$10$OIEEvVgT07S2JGAIy3S0XeMW0NacF1mdhV4FAGbnq8KCOFZdYhupy',
            'money'     => 100
        ]
    ];
}
