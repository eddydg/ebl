<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProductsFixture
 *
 */
class ProductsFixture extends TestFixture
{
    public $import = ['model' => 'product', 'records' => true];

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'category' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'date_start' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'date_end' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'price_start' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'price_instant' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'is_over' => ['type' => 'integer', 'length' => null, 'null' => false, 'default' => false, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'producusers_fk_idx' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'producusers_fk' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */

    public $records = [
        [
            'user_id' => 1,
            'title' => 'Jouet pour chien',
            'description' => 'Pour un max de défoulement',
            'category' => 'Animalerie',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 1.5,
            'price_instant' => 1.5,
            'is_over' => false
        ],
        [
            'user_id' => 2,
            'title' => 'Marteau DEXTER',
            'description' => 'Travaux de menuiserie',
            'category' => 'Bricolage',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 3,
            'price_instant' => 10,
            'is_over' => false
        ],
        [
            'user_id' => 3,
            'title' => 'Stan smith ADIDAS',
            'description' => 'Pointure 35 pour femme',
            'category' => 'Chaussures',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 40,
            'price_instant' => 100,
            'is_over' => false
        ],
        [
            'user_id' => 4,
            'title' => 'Le Seigneur des Anneaux',
            'description' => 'Le retour du roi',
            'category' => 'DVD, cinéma',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 1,
            'price_instant' => 10,
            'is_over' => false
        ],
        [
            'user_id' => 5,
            'title' => 'Machine à laver',
            'description' => 'SAMSUNG, bon état',
            'category' => 'Électroménager',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 200,
            'price_instant' => 600,
            'is_over' => false
        ],
        [
            'user_id' => 6,
            'title' => 'Ecran Asus',
            'description' => '21 pouces',
            'category' => 'Informatique',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 100,
            'price_instant' => 150,
            'is_over' => false
        ],
        [
            'user_id' => 7,
            'title' => 'Death Note tome 1',
            'description' => 'Très bon manga, tout neuf',
            'category' => 'Livres',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 1,
            'price_instant' => 7,
            'is_over' => false
        ],
        [
            'user_id' => 8,
            'title' => 'Guitare électrique',
            'description' => 'Bon état',
            'category' => 'Musique',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 200,
            'price_instant' => 300,
            'is_over' => false
        ],
        [
            'user_id' => 9,
            'title' => 'Brosse à dents connecté',
            'description' => 'Jamais ouvert',
            'category' => 'Salle de bain',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 50,
            'price_instant' => 100,
            'is_over' => false
        ],
        [
            'user_id' => 10,
            'title' => 'Ballon de foot EURO 2016',
            'description' => 'Tout neuf',
            'category' => 'Sport',
            'date_start' => '2016-07-02 22:29:00',
            'date_end' => '2016-07-09 22:29:00',
            'price_start' => 10,
            'price_instant' => 15,
            'is_over' => false
        ],
    ];

}
