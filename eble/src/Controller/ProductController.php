<?php
namespace App\Controller;
use Cake\I18n\Time;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Date;

/**
* Product Controller
*
* @property \App\Model\Table\ProductTable $Product
*/
class ProductController extends AppController
{

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('User');
    }

    /**
    * Index method
    *
    * @return \Cake\Network\Response|null
    */
    public function index($id = null)
    {
        $user_id = $this->Auth->user('id');
        $product;
        if ($id == null)
            $product = $this->Product->find()
                ->where(['date_end >' => Time::now(), 'is_over' != 1])->all();
        else
        {
            $ids = $this->Product->find()->select('id')
                    ->all();
            $arrid = array();
            foreach ($ids as $prd)
                array_push($arrid, $prd->id);
            if (!in_array($id, $arrid))
                return $this->redirect(['controller' => 'Pages', 'action' => 'display']);

            else
            {
                $product = $this->Product->find('all')
                            ->where(['id' => $id]);
                $this->set('lastbid', $this->getLastBid($id));

                $bids = $this->getProductBids($id);
                $this->set('bids', $bids);
            }
        }
        $this->set('user_id', $user_id);
        $this->set('product', $product);
    }

    /**
    * View method
    *
    * @param string|null $id Product id.
    * @return \Cake\Network\Response|null
    * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function view($id = null)
    {
        $product = $this->Product->get($id, [
            'contain' => ['User']
        ]);

        $this->set(compact('product'));
        $this->set('_serialize', array('product'));
    }

    /**
    * Add method
    *
    * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
    */
    public function add()
    {
        $product = $this->Product->newEntity();
        
        if ($this->request->is('post')) {

            $date_end = date("Y-m-d H:i:s", strtotime($this->request->data['date_end']));
            $date_start = new \DateTime('now', new \DateTimeZone('Europe/Paris'));
            $datetime_end = new \DateTime($date_end,  new \DateTimeZone('Europe/Paris'));
            $this->request->data['date_end'] = $datetime_end->format('Y-m-d H:i:s');
            $this->request->data['date_start'] = $date_start->format('Y-m-d H:i:s');;
            $this->request->data['is_over'] = 0;

            if (empty($this->request->data['description']))
                $this->request->data['description'] = "Aucune description";

            if (empty($this->request->data['title']))
                $this->Flash->error(__('Le titre ne peut pas être vide.'));

            else if (!is_float(floatval($this->request->data['price_start']))
                    && !is_int(intval($this->request->data['price_start'])))
                $this->Flash->error(__('Le prix de départ doit être un nombre décimal.'));

            else if (!is_float(floatval($this->request->data['price_instant']))
                    && !is_int(intval($this->request->data['price_instant'])))
                $this->Flash->error(__('Le prix d\'achat immédiat doit être un nombre décimal.'));

            else if ($this->request->data['price_instant'] <= $this->request->data['price_start'])
                $this->Flash->error(__('Le prix d\'achat immédiat doit être strictement supérieur au prix de départ.'));

            else {

                $product = $this->Product->patchEntity($product, $this->request->data);

                if (!$product->check()){
                    $this->Flash->error(__('La date de fin ne peut pas être supérieur à la date de début.'));
                }
                else if ($product->check() && $this->Product->save($product)) {
                    $this->Flash->success(__('Le produit a bien été enregistré.'));
                    return $this->redirect(['action' => 'add']);
                } else {
                    $this->Flash->error(__('Le produit n\'a pas pu être enregistré. Veuillez réessayer.'));
                }
            }
        }
        $user = $this->Product->User->find('list', ['limit' => 200]);
        $this->set(compact('product', 'user'));
        $this->set('_serialize', ['product']);
        return;
    }

    /**
    * Edit method
    *
    * @param string|null $id Product id.
    * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
    * @throws \Cake\Network\Exception\NotFoundException When record not found.
    */
    public function edit($id = null)
    {
        $product = $this->Product->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Product->patchEntity($product, $this->request->data);
            if ($this->Product->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $user = $this->Product->User->find('list', ['limit' => 200]);
        $this->set(compact('product', 'user'));
        $this->set('_serialize', ['product']);
    }

    /**
    * Delete method
    *
    * @param string|null $id Product id.
    * @return \Cake\Network\Response|null Redirects to index.
    * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Product->get($id);
        if ($this->Product->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function search()
    {
        $query = trim($this->request->query['srch-term']);
        $category_query = trim($this->request->query['category']);
        $results = array();
        if ($category_query === 'all')
        {
            if (strlen($query) > 0)
            {
                $query = htmlspecialchars($query);
                $query_words = explode(' ', $query);
                foreach ($this->Product->find('all') as $product)
                {
                    $exp = '/'.implode('|', array_map('preg_quote', $query_words)).('/i');
                    if (preg_match($exp, $product['title']))
                        array_push($results, $product);
                }
            }
        }
        else
        {
            $results = $this->Product->find('all')
                            ->where(['category =' => $category_query]);
            if (strlen($query) > 0)
            {
                $res = array();
                $query = htmlspecialchars($query);
                $query_words = explode(' ', $query);
                foreach ($results as $product)
                {
                    $exp = '/'.implode('|', array_map('preg_quote', $query_words)).('/i');
                    if (preg_match($exp, $product['title']))
                        array_push($res, $product);
                }
                $results = $res;
            }
        }
        $this->set('results', $results);
    }

    public function productbids($product_id)
    {
        $this->loadModel('User');
        $query = $this->getProductBids($product_id);
        $product = $this->Product->get($product_id);
        $users_bids = array();
        foreach ($query as $bid)
        {
            $user = $this->User->get($bid['user_id']);
            $user_bid =  array(
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'date' => $bid['date'],
                'amount' => $bid['amount']
            );
            array_push($users_bids, $user_bid);
        }
        $this->set('product_id', $product_id);
        $this->set('product_end', $product['date_end']);
        $this->set('users_bids', $users_bids);
    }

    public function getProductBids($product_id)
    {
        $this->loadModel('Bid');
        $query = $this->Bid->find('all')
                ->where(['product_id =' => $product_id])
                ->orderDesc('amount');
        return $query;
    }

    public function getLastBid($productid)
    {
        $this->loadModel('Bid');
        $query = $this->Bid->find('all')
                ->where(['product_id =' => $productid])
                ->orderDesc('amount')->first();
        if ($query == null)
            return;
        return $query->amount;
    }
}