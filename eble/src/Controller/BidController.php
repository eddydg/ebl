<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Bid Controller
 *
 * @property \App\Model\Table\BidTable $Bid
 */
class BidController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['User', 'Product']
        ];
        $bid = $this->paginate($this->Bid);

        $this->set(compact('bid'));
        $this->set('_serialize', ['bid']);
    }

    /**
     * View method
     *
     * @param string|null $id Bid id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bid = $this->Bid->get($id, [
            'contain' => ['User', 'Product']
        ]);

        $this->set('bid', $bid);
        $this->set('_serialize', ['bid']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($prd_id)
    {
        $this->loadModel('Product');
        $this->loadModel('User');

        $product = $this->Product->get($prd_id);
        $bid = $this->Bid->newEntity();
        if ($this->request->is('post')) {
            $bid = $this->Bid->patchEntity($bid, $this->request->data);
            if ($bid->amount <= $this->getLastBid($prd_id))
            {
                $this->Flash->error ("Veuillez entrer un montant strictement supérieur à l'enchère actuelle");
                return $this->redirect(['controller' => 'Product','action' => '/' . $prd_id]);
            }
            else if ($product->price_start > $bid->amount)
            {
                $this->Flash->error ("Veuillez entrer un montant supérieur au montant minimum demandé par l'eBleur");
                return $this->redirect(['controller' => 'Product','action' => '/' . $prd_id]);
            }
            else
            {
                $user_id = $this->Auth->user('id');
                $user = $this->User->get($user_id);
                $lbid = $this->Bid->find()
                       ->where(['user_id' => $user_id, 'product_id' => $prd_id])
                        ->orderDesc('amount')->first();
                //$this->set('<br />'.var_dump($lbid->amount));
                if ($lbid == null)
                    $diff = $bid->amount;
                else
                    $diff = $bid->amount - $lbid->amount;
                //$this->set('<br />'.var_dump($lbid->amount));
                if ($user->money - $diff < 0)
                {
                    $this->Flash->error ("Vous n'avez pas assez approvisionné votre compte pour effectuer cette enchère");
                    return $this->redirect(['controller' => 'Product','action' => '/' . $prd_id]);
                }
                else if ($this->Bid->save($bid))
                {
                    $user->money -= $diff;
                    $this->User->save($user);
                    $this->Flash->success(__('Enchère enregistrée avec succès'));
                    return $this->redirect(['controller' => 'Product','action' => '/' . $prd_id]);
                }
                else
                    $this->Flash->error(__('Un soucis est survenu, veuillez réessayer'));
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Bid id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bid = $this->Bid->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bid = $this->Bid->patchEntity($bid, $this->request->data);
            if ($this->Bid->save($bid)) {
                $this->Flash->success(__('The bid has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bid could not be saved. Please, try again.'));
            }
        }
        $user = $this->Bid->User->find('list', ['limit' => 200]);
        $product = $this->Bid->Product->find('list', ['limit' => 200]);
        $this->set(compact('bid', 'user', 'product'));
        $this->set('_serialize', ['bid']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bid id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bid = $this->Bid->get($id);
        if ($this->Bid->delete($bid)) {
            $this->Flash->success(__('The bid has been deleted.'));
        } else {
            $this->Flash->error(__('The bid could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function instantBuy($productId)
    {
        $userId = $this->Auth->user('id');


        $this->loadModel('User');
        $this->loadModel('Product');
        $user = $this->Bid->User->get($userId);
        $product = $this->Bid->Product->get($productId);
        $priceInstant = $product->get('price_instant');

        if ($product->get('is_over')) {
            $this->Flash->error(__('Le produit a déjà été vendu.'));
            return $this->redirect(['controller'=> 'Pages', 'action' => 'index']);
        }

        if ($priceInstant == 0) {
            $this->Flash->error(__('Ce produit ne permet pas l\achat instantané.'));
            return $this->redirect(['controller'=> 'Pages', 'action' => 'index']);
        }

        if ($user->get('money') - $priceInstant < 0) {
            $this->Flash->error(__('Vous n\'avez pas suffisamment de blé.'));
            return $this->redirect(['controller'=> 'Pages', 'action' => 'index']);
        }
        $user->set('money', $user->get('money') - $priceInstant);

        $bidTable = TableRegistry::get('Bid');
        $bid = $bidTable->newEntity();

        $bid->set('is_instant', true);
        $bid->set('date', date('Y-m-d H:i:s'));
        $bid->set('amount', $priceInstant);
        $bid->set('user', $user);
        $bid->set('product', $product);

        $product->set('is_over', true);

        if ($this->Bid->save($bid) && $this->Bid->Product->save($product) && $this->Bid->User->save($user)) {
            $this->Flash->success(__('Achat instantané effectué.'));
        } else {
            $this->Flash->error(__('Une erreur est survenue en faisant l\'achat instantané.'));
        }

        return $this->redirect(['controller'=> 'Pages', 'action' => 'index']);
    }
    
    public function getLastBid($productid)
    {
        $query = $this->Bid->find('all')
                ->where(['product_id =' => $productid])
                ->orderDesc('amount')->first();
        return $query->amount;
    }
}
