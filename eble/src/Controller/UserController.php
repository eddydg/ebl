<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;


/**
 * User Controller
 *
 * @property \App\Model\Table\UserTable $User
 */
class UserController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $id = $this->Auth->user('id');
        
        $user = $this->User->get($id, [
            'contain' => []
        ]);

        $this->set(compact('user'));
        $this->set('_serialize', array('user'));

        $history = $this->history();
        $this->set('history', $history);

        $sales = $this->sales();
        $this->set('sales', $sales);

        $in_progress = $this->involvedbids();
        $this->set('products', $in_progress);
        
        $activeSales = $this->activeSales();
        $this->set('activeSales', $activeSales);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->User->get($id, [
            'contain' => []
        ]);

        $this->set(compact('user'));
        $this->set('_serialize', array('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->User->newEntity();
        if ($this->request->is('post')) {
            $user = $this->User->patchEntity($user, $this->request->data);
            if ($this->User->save($user)) {
                $this->Flash->success(__('Vous êtes inscrit !'));
                return $this->redirect(['controller' => 'Pages', 'action' => 'index']);
            } else {
                $this->Flash->error(__('Impossible de créer le compte. Veuillez réessayer.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->User->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->User->patchEntity($user, $this->request->data);
            if ($this->User->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->User->get($id);
        if ($this->User->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'logout']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {

                if ($this->request->data['User']['remember_me'] == 1) {
                // write the cookie
                $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '1 weeks');
            }
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Email ou mot de passe invalides. Veuillez réessayer'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
        
    public function involvedbids()
    {
        $connection = ConnectionManager::get('default');
        $user_id = $this->Auth->user('id');
        $this->loadModel('Product');
        $this->loadModel('Bid');
        $results = $connection
        ->execute(
        '
select maxi.*, b.user_id, b.amount as amount, max(b.amount) as max_amount,
            b.pt as title, b.pd as description, b.c  as category,
            b.pde as date_end, b.pi as id from
            (
                select max(amount) as max, product_id
                from bids
                group by product_id
            ) as maxi
            inner join (
                select bids.*, products.title as pt, products.description as pd, products.category  as c,
                products.date_end as pde, products.id as pi
                from bids
                inner join products on products.id = bids.product_id
                where bids.user_id = :user_id and date_end > now()
            ) as b on maxi.product_id = b.product_id GROUP by product_id ORDER BY b.pde;',
        ['user_id' => $user_id]
    )
    ->fetchAll('assoc');
        return $results;
    }

    public function history() {
        $connection = ConnectionManager::get('default');
        $user_id = $this->Auth->user('id');
        $this->loadModel('Product');
        $this->loadModel('Bid');
        $history = $connection
        ->execute(
        'select * from
        (
            select bids.id as bid_id, bids.amount as am, bids.is_instant as ii, bids.date as bought_date,
            products.id as prd_id, products.date_end as date_end, products.title as title, products.category as category
        from bids
        inner join products on bids.product_id = products.id
        where bids.user_id = :user_id
        ) as userbid
        where 
        userbid.ii = 1 OR
        userbid.date_end < now() AND NOT
        1 in (SELECT bids.is_instant from bids inner join products on bids.product_id = products.id where products.id = prd_id) AND
        userbid.am in (select max(amount) from bids where bids.product_id = userbid.prd_id)
        ;',
        ['user_id' => $user_id]
        )
        ->fetchAll('assoc');
        return $history;
    }
    
    
    public function sales() {
        $connection = ConnectionManager::get('default');
        $user_id = $this->Auth->user('id');
        $this->loadModel('Product');
        $this->loadModel('Bid');
        $sales = $connection
        ->execute(
        'select products.id as prd_id,
            products.title as title,
            products.category as category,
            products.description as description,
            products.date_end,
            bids.id as bid_id,
            bids.amount as amount,
            max(bids.amount) as am,
            bids.date as date,
            bids.is_instant from products
        left outer join bids on products.id  = bids.product_id
        where products.user_id = :user_id and (products.date_end < now() or products.is_over = 1) group by prd_id;',
        ['user_id' => $user_id]
        )
        ->fetchAll('assoc');
        return $sales;
    }
    
    public function activeSales()
    {
        $connection = ConnectionManager::get('default');
        $user_id = $this->Auth->user('id');
        $this->loadModel('Product');
        $this->loadModel('Bid');
        $result = $connection
        ->execute(
        'select products.*, max(bids.amount) as am from products left join bids on bids.product_id = products.id
            where products.user_id = :user_id and products.date_end > now() and products.is_over != 1
            GROUP by products.id;',
        ['user_id' => $user_id]
        )
        ->fetchAll('assoc');
        return $result;
    }
}
