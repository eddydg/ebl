<?php
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.10.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;

$this->layout = false;

if (!Configure::read('debug')):
  throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
  <?= $this->Html->script('jquery-3.0.0.min.js') ?>
  <?= $this->Html->script('moment.js') ?>
  <?= $this->Html->script('bootstrap.min.js') ?>
  <?= $this->Html->script('datetimepicker.min.js') ?>
  <?= $this->Html->charset() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?= $this->Html->meta('icon') ?>
  <?= $this->Html->css('base.css') ?>
  <?= $this->Html->css('bootstrap.min.css') ?>
  <?= $this->Html->css('custom.css') ?>
  <?= $this->Html->css('user.css') ?>
  <?= $this->Html->css('font-awesome.min.css') ?>
  <?= $this->Html->css('datetimepicker.min.css') ?>


</head>
<body>
  <nav class="header fadein transparent">
    <div class="wrapper">
      <div class="inner">
        <a class="logo" href="<?= Router::url('/'); ?>"></a>
        <div class="left">
          <a class="login" data-ga-action="Log in" data-ga-category="Homepage Navigation Header" href="<?= Router::url('/'); ?>"><span class="glyphicon glyphicon-th"></span>À la une</a>
          <a class="login" data-ga-action="Log in" data-ga-category="Homepage Navigation Header" href="<?= Router::url('/product/add'); ?>"><span class="fa fa-product-hunt"></span>Vendre un produit</a>
          <div class="search-bar pull-right">
            <form class="navbar-form" role="search" action="<?= Router::url('/product/search'); ?>">
              <div class="input-group input-group-sm">
                  <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    	<span id="search_drop">Filtrer</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#all">Tout</a></li>
                      <li><a href="#Animalerie">Animalerie</a></li>
                      <li><a href="#Bricolage">Bricolage</a></li>
                      <li><a href="#Chaussures">Chaussures</a></li>
                      <li><a href="#DVD, cinéma">DVD, cinéma</a></li>
                      <li><a href="#Électroménager">Électroménager</a></li>
                      <li><a href="#Informatique">Informatique</a></li>
                      <li><a href="#Livres">Livres</a></li>
                      <li><a href="#Musique">Musique</a></li>
                      <li><a href="#Salle de bain">Salle de bain</a></li>
                      <li><a href="#Sport">Sport</a></li>
                    </ul>
                </div>
                <input type="hidden" name="category" value="all" id="category">
                <input type="text" class="form-control input-large" placeholder="Rechercher un produit" name="srch-term" id="srch-term">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="right">
          <?php if (!$this->request->session()->read('Auth.User.id')): ?>
            <a class="login" data-ga-action="Sign up" data-ga-category="Homepage Navigation Header" href="<?= Router::url('/user/add'); ?>">Inscription</a>
            <a class="login" data-ga-action="Log in" data-ga-category="Homepage Navigation Header" href="<?= Router::url('/user/login'); ?>">Se connecter</a>
          <?php else: ?>
            <a class="login" data-ga-action="Log Out" data-ga-category="Homepage Navigation Header" href="<?= Router::url('/user/logout'); ?>">Se Déconnecter</a>
            <a class="login" data-ga-action="Account" data-ga-category="Homepage Navigation Header" href="<?= Router::url('/user/'); ?>">Mon Compte</a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </nav>
  <script type="text/javascript">
    $(document).ready(function(e){
        $('.search-panel .dropdown-menu').find('a').click(function(e)
        {
          e.preventDefault();
          var category = $(this).attr("href").replace("#","");
          var innerButton = $(this).text();
         $('.search-panel span#search_drop').text(innerButton);
         $('.input-group #category').val(category);
     });
    });
  </script>

  <div class="container-fluid" id="container">
    <?= $this->fetch('content'); ?>
  </div>
</body>
</html>
