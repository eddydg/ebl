<div class="" id="content">
    <div class="users form form-box">
        <?= $this->Flash->render('auth') ?>
        <?= $this->Form->create() ?>
        <fieldset>
            <legend><?= __('Se connecter') ?></legend>
            <br>
            <?= $this->Flash->render() ?>
            <br>
            <?= $this->Form->input('email', array('label' => 'E-mail')); ?>
            <?= $this->Form->input('password', array('label' => 'Mot de passe')); ?>
            <?= $this->Form->checkbox('remember_me'); ?> Se souvenir de moi
        </fieldset>
        <?= $this->Form->button(__('Se connecter')); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>

