<div class="" id="content">
    <div class="users form form-box">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('S\'inscrire') ?></legend>
            <br>
            <?= $this->Flash->render() ?>
            <br>
            <?php
            echo $this->Form->input('email');
            echo $this->Form->input('firstname');
            echo $this->Form->input('lastname');
            echo $this->Form->input('password');
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit')) ?>
        <?= $this->Form->end() ?>
    </div>
</div>