
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Firstname') ?></th>
            <td><?= h($user->firstname) ?></td>
        </tr>
        <tr>
            <th><?= __('Lastname') ?></th>
            <td><?= h($user->lastname) ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Bids') ?></h4>
        <?php if (!empty($user->bids)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Amount') ?></th>
                <th><?= __('Is Instant') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->bids as $bids): ?>
            <tr>
                <td><?= h($bids->id) ?></td>
                <td><?= h($bids->user_id) ?></td>
                <td><?= h($bids->product_id) ?></td>
                <td><?= h($bids->date) ?></td>
                <td><?= h($bids->amount) ?></td>
                <td><?= h($bids->is_instant) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bids', 'action' => 'view', $bids->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bids', 'action' => 'edit', $bids->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bids', 'action' => 'delete', $bids->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bids->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Products') ?></h4>
        <?php if (!empty($user->products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Category') ?></th>
                <th><?= __('Date Start') ?></th>
                <th><?= __('Date End') ?></th>
                <th><?= __('Price Start') ?></th>
                <th><?= __('Price Instant') ?></th>
                <th><?= __('Is Over') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->products as $products): ?>
            <tr>
                <td><?= h($products->id) ?></td>
                <td><?= h($products->user_id) ?></td>
                <td><?= h($products->title) ?></td>
                <td><?= h($products->description) ?></td>
                <td><?= h($products->category) ?></td>
                <td><?= h($products->date_start) ?></td>
                <td><?= h($products->date_end) ?></td>
                <td><?= h($products->price_start) ?></td>
                <td><?= h($products->price_instant) ?></td>
                <td><?= h($products->is_over) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
