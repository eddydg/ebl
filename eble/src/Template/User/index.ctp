

<!-- <div id="content">
    <h3>Infos générales</h3>
    <p>
        <b><?= __('E-mail') ?></b>: <?= h($user->email) ?> <br>
        <b><?= __('Prénom') ?></b>: <?= h($user->firstname) ?> <br>
        <b><?= __('Nom') ?></b>: <?= h($user->lastname) ?> <br>
        <b><?= __('Crédit restant') ?></b>: <?= h($user->money) ?>€ <br>
    </p>
    <div class="related">
        <h4><?= __('Related Bids') ?></h4>
        <?php if (!empty($user->bids)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('User Id') ?></th>
                    <th><?= __('Product Id') ?></th>
                    <th><?= __('Date') ?></th>
                    <th><?= __('Amount') ?></th>
                    <th><?= __('Is Instant') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($user->bids as $bids): ?>
                    <tr>
                        <td><?= h($bids->id) ?></td>
                        <td><?= h($bids->user_id) ?></td>
                        <td><?= h($bids->product_id) ?></td>
                        <td><?= h($bids->date) ?></td>
                        <td><?= h($bids->amount) ?></td>
                        <td><?= h($bids->is_instant) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['controller' => 'Bids', 'action' => 'view', $bids->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['controller' => 'Bids', 'action' => 'edit', $bids->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bids', 'action' => 'delete', $bids->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bids->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Products') ?></h4>
        <?php if (!empty($user->products)): ?>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th><?= __('Id') ?></th>
                    <th><?= __('User Id') ?></th>
                    <th><?= __('Title') ?></th>
                    <th><?= __('Description') ?></th>
                    <th><?= __('Category') ?></th>
                    <th><?= __('Date Start') ?></th>
                    <th><?= __('Date End') ?></th>
                    <th><?= __('Price Start') ?></th>
                    <th><?= __('Price Instant') ?></th>
                    <th><?= __('Is Over') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($user->products as $products): ?>
                    <tr>
                        <td><?= h($products->id) ?></td>
                        <td><?= h($products->user_id) ?></td>
                        <td><?= h($products->title) ?></td>
                        <td><?= h($products->description) ?></td>
                        <td><?= h($products->category) ?></td>
                        <td><?= h($products->date_start) ?></td>
                        <td><?= h($products->date_end) ?></td>
                        <td><?= h($products->price_start) ?></td>
                        <td><?= h($products->price_instant) ?></td>
                        <td><?= h($products->is_over) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </div>

    <div class="col-md-6">
        <h2>Mes Achats</h2>
            <div class="list-group">
                <?php

                    if (count($history) > 0)
                    {
                        foreach ($history as $h)
                        { 
                            $date = date("d/m/Y H:i", strtotime($h['bought_date']));
                            $date_end = explode(" ", $date)[0];
                            $date_end_h = explode(" ", $date)[1];
                        ?>
                            <div class="item col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-group-item">
                                            <a class="bid-item" href="/<?php echo $h['prd_id'] ?>">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h3 class="group inner list-group-item-heading"><?php echo $h['title']; ?></h3>
                                        <p class="max-text-with"><?php echo $h['category']; ?></p>
                                        <hr />
                                        <div class="group inner list-group-item-text italic max-text-with">
                                        Acheté le <strong><?php echo $date_end; ?></strong> à <strong><?php echo $date_end_h; ?></strong> </div>
                                                        <div class="group inner list-group-item-text">à : <span class="price"><?php echo $h['am']; ?>€</span></div>
                                        <a class="btn btn-warning" data-toggle="modal" onclick="alert(<?php echo $h['prd_id']; ?>);">Détails</a>
                                    </div>
                                </div>
                                            </a>

                            </div>
                        <?php
                        }
                    } else {
                         ?>
                            <div class="main_title text_wrap">
                                <h1>Vous n'avez pas d'historique d'achats</h1>
                            </div>
                        <?php
                    }
                ?>
            </div>
    </div>

    <div class="col-md-6">
         <h2>En cours</h2>
            <div class="list-group">
                <?php

                    if (count($products) > 0)
                    {
                        foreach ($products as $product)
                        { 
                            $date = date("d/m/Y H:i", strtotime($product['date_end']));
                            $date_end = explode(" ", $date)[0];
                            $date_end_h = explode(" ", $date)[1];
                        ?>
                            <div class="item col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-group-item">
                                            <a class="bid-item" href="product/view/<?php echo $product['id'] ?>">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h3 class="group inner list-group-item-heading"><?php echo $product['title']; ?></h3>
                                        <p class="max-text-with"><?php echo $product['category']; ?></p>
                                        <hr />
                                        <div class="group inner list-group-item-text italic max-text-with">
                                        Fin le <strong><?php echo $date_end; ?></strong> à <strong><?php echo $date_end_h; ?></strong> </div>
                                        <div class="group inner list-group-item-text">Enchère à <span class="price"><?php echo $product['max']; ?>€</span></div>
                                                        <div class="group inner list-group-item-text">Votre dernière enchère : <span class="price"><?php echo $product['max_amount']; ?>€</span></div>
                                        <a class="btn btn-warning" data-toggle="modal" onclick="alert(<?php echo $product['id']; ?>);">Détails</a>
                                    </div>
                                </div>
                                            </a>

                            </div>
                        <?php
                        }
                    } else {
                         ?>
                            <div class="main_title text_wrap">
                                <h1>Vous n'avez aucuns achats/ventes en cours</h1>
                            </div>
                        <?php
                    }
                ?>
            </div>
    </div>
</div> -->


 <script>
     $(document).ready(function() {
        
        console.log("ready!");

        $('#tab-user a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            console.log("CLICKED !");
        });        

    });            
</script>

<div id="content">
    <div class="col-md-offset-2 col-md-8 col-md-offset-2">
                
        <div>

            <div class="container">
                <ul class="nav nav-tabs" id="tab-user">
                    <li class="active"><a href="#infos">Informations Générales</a></li>
                    <li><a href="#inProgress">En cours</a></li>
                    <li><a href="#bids">Mes achats</a></li>
                    <li><a href="#histo">Vendu</a></li>
                    <li><a href="#active">Ventes en cours</a></li>

                </ul>
           </div>
 
            <div class="tab-content">

                <!-- INFOS SUR L'UTILISATEUR ICI -->
                <div class="tab-pane active" id="infos" style="margin-left: 25px;">
                      <div class="form-horizontal">
                                                                  
                          <div class="form-inline" style="padding: 20px;">
                              <div class="form-group">
                                  <label for="Email"><strong>Email :</strong></label>
                                  <label><?= h($user->email) ?></label>
                              </div>
                          </div>
                          <div class="form-inline" style="padding: 20px;">
                              <div class="form-group">
                                  <label for="Prénom"><strong>Prénom :</strong></label>
                                  <label><?= h($user->firstname) ?></label>
                              </div>
                          </div>
                          <div class="form-inline" style="padding: 20px;">
                              <div class="form-group">
                                  <label  for="Nom"><strong>Nom :</strong></label>
                                  <label><?= h($user->lastname) ?></label>
                              </div>
                          </div>
                          <div class="form-inline" style="padding: 20px;">
                              <div class="form-group">
                                  <label for="credits"><strong>Crédit restant :</strong></label>
                                  <label><?= h($user->money) ?>€</label>
                              </div>
                          </div>
                          
                     </div>
                </div>
                <!-- FIN INFOS SUR L'UTILISATEUR ICI -->

                <!-- ACHATS EN COURS DE L'UTILISATEUR ICI -->
                <div class="tab-pane" id="inProgress">
                    <?php
                        if (count($products) > 0) {

                            ?>
                                <div class="col-md-12">
                                    <div class="" style="padding-top:50px">
                                       <?php
                                            foreach ($products as $product)
                                            {
                                                $date = date("d/m/Y H:i", strtotime($product['date_end']));
                                                $date_end = explode(" ", $date)[0];
                                                $date_end_h = explode(" ", $date)[1];

                                                $dStart = new DateTime();
                                                $dEnd  = new DateTime(date("Y-m-d H:i", strtotime($product['date_end'])));
                                                $dDiff = $dStart->diff($dEnd);

                                                ?>
                                                   
                                                    <div class="col-sm-6 col-lg-4 col-md-5">
                                                         <a class="bid-item" href="../product/<?php echo $product['id'] ?>">
                                                           <div class="thumbnail">
                                                               <div>
                                                                   <label class="label-table">Il reste <?php echo $dDiff->days + 1; ?> jours</label>
                                                                   <br />
                                                                   <label class="label-table">Fin le <strong><?php echo $date_end; ?></strong> à <strong><?php echo $date_end_h ?></strong></label>
                                                               </div>
                                                               <hr />
                                                               <div class="caption">
                                                                   <h4><?php echo $product['title']; ?></h4>
                                                                    <div class="progressDescritpionDiv"><?php echo $product['description']; ?></div>
                                                               </div>
                                                               <?php 

                                                                $diff = $product['max_amount'] - $product['max'];
                                                                if ($diff < 0) {
                                                                ?>
                                                                    <td class="list_item_account">
                                                                        <div>
                                                                            <label class="label-table danger">Enchère actuelle : <span class="price"><?php echo $product['max']; ?>€</span></label>
                                                                            <br />
                                                                            <label class="label-table danger">Votre enchère : <span class="price"><?php echo $product['max_amount']; ?>€</span></label>
                                                                        </div>
                                                                    </td>
                                                                <?php
                                                                }
                                                                else {
                                                                    ?>
                                                                     <td class="list_item_account">
                                                                        <div> 
                                                                            <label class="label-table danger">Enchère actuelle : <span class="price"><?php echo $product['max']; ?>€</span></label>
                                                                            <br />
                                                                            <label class="label-table success">Votre enchère : <span class="price"><?php echo $product['max_amount']; ?>€</span></label>
                                                                        </div>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <div class="caption">
                                                                    <a class="btn btn-info" data-toggle="modal" id="details" href="../product/<?php echo $product['id']; ?>">Détails</a>
                                                                    <br />
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                <?php
                                            }
                                       ?>
                                    </div>
                                </div>
                            <?php
                        } else {
                            ?>
                                <div><h2>Vous n'avez pas d'achats en cours</h2></div>
                            <?php
                        }
                    ?>
                </div>
                <!-- FIN ACHATS EN COURS DE L'UTILISATEUR ICI -->

                <!-- ACHATS DE L'UTILISATEUR ICI -->
                <div class="tab-pane" id="bids">
                    <?php

                        if (count($history) > 0)
                        {
                            ?>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-container">
                                    <table class="table table-filter">
                                     <thead>
                                        <tr>
                                            <th class="resource-name">Produit</th>
                                            <th class="resource-name">Catégorie</th>
                                            <th class="resource-name">Achats</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody> 
                                            <?php
                                                foreach ($history as $h)
                                                { 
                                                    $date = date("d/m/Y H:i", strtotime($h['bought_date']));
                                                    $date_end = explode(" ", $date)[0];
                                                    $date_end_h = explode(" ", $date)[1];
                                                ?>
                                                     <tr data-status='En cours'>
                                                        <td class="list_item_account"><div><label class="label-table"><?php echo $h['title']; ?></label></div></td>
                                                        <td class="list_item_account"><div><label class="label-table"><?php echo $h['category']; ?></label></div></td>
                                                        <td class="list_item_account"><div class='media'>
                                                            <div>
                                                                <label class="label-table">
                                                                    <strong><?php echo $date_end; ?></strong>
                                                                    à <strong><?php echo $date_end_h; ?>
                                                                    pour : <span class="price"><?php echo $h['am']; ?>€</strong>
                                                                </label>
                                                            </div>
                                                            </div>
                                                        </td>
                                                        <td class="list_item_account"><div>
                                                            <a class="btn btn-info" data-toggle="modal" href="../product/<?php echo $h['prd_id']; ?>">Détails</a>
                                                            </div>
                                                        </td>
                                                <?php
                                                } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                        } else {
                             ?>
                                <div>
                                    <h2>Vous n'avez pas d'historique d'achats</h2>
                                </div>
                            <?php
                        }
                    ?>
                </div>
                <!-- FIN ACHATS DE L'UTILISATEUR ICI -->

                <!-- VENTES DE L'UTILISATEUR ICI -->
                <div class="tab-pane" id="histo">
                    <?php

                        if (count($sales) > 0)
                        {
                            ?>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-container">
                                    <table class="table table-filter">
                                    <thead>
                                        <tr>
                                            <th class="resource-name">Produit</th>
                                            <th class="resource-name">Catégorie</th>
                                            <th class="resource-name">Vente</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody> 
                                            <?php
                                                foreach ($sales as $h)
                                                { 
                                                    $date = date("d/m/Y H:i", strtotime($h['date']));
                                                    $date_end = explode(" ", $date)[0];
                                                    $date_end_h = explode(" ", $date)[1];
                                                ?>
                                                     <tr data-status='En cours'>
                                                        <td class="list_item_account"><div><label class="label-table"><?php echo $h['title']; ?></label></div></td>
                                                        <td class="list_item_account"><div><label class="label-table"><?php echo $h['category']; ?></label></div></td>
                                                        <td class="list_item_account"><div class='media'>
                                                            <div>
                                                                <?php if ($h['am'] != null)
                                                                {?>
                                                                <label class="label-table">
                                                                    <strong><?php echo $date_end; ?></strong>
                                                                    à <strong><?php echo $date_end_h; ?>
                                                                    pour : <span class="price"><?php echo $h['am']; ?>€</strong>
                                                                </label>
                                                                <?php
                                                                }
                                                                else {
                                                                ?>
                                                                    <label class="label-table">
                                                                        Article invendu
                                                                    </label>

                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            </div>
                                                        </td>
                                                        <td class="list_item_account"><div>
                                                            <a class="btn btn-info" data-toggle="modal" href="../product/<?php echo $h['prd_id']; ?>">Détails</a>
                                                            </div>
                                                        </td>
                                                <?php
                                                } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                        } else {
                             ?>
                                <div>
                                    <h2>Vous n'avez pas d'historique de ventes</h2>
                                </div>
                            <?php
                        }
                    ?>
                </div>
                <!-- FIN VENTES DE L'UTILISATEUR ICI -->
                
                
                <div class="tab-pane" id="active">
                    <?php

                        if (count($activeSales) > 0)
                        {
                            ?>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-container">
                                    <table class="table table-filter">
                                    <thead>
                                        <tr>
                                            <th class="resource-name">Produit</th>
                                            <th class="resource-name">Catégorie</th>
                                            <th class="resource-name">Enchère actuelle</th>
                                            <th class="resource-name">Jusqu'au</th>

                                            <th></th>
                                        </tr>
                                    </thead>
                                        <tbody> 
                                            <?php
                                                foreach ($activeSales as $h)
                                                { 
                                                    $date = date("d/m/Y H:i", strtotime($h['date_end']));
                                                    $date_end = explode(" ", $date)[0];
                                                    $date_end_h = explode(" ", $date)[1];
                                                    $amout = $h['am'];
                                                    if ($amout == null)
                                                        $amout = 0;
                                                ?>
                                                     <tr data-status='En cours'>
                                                        <td class="list_item_account"><div><label class="label-table"><?php echo $h['title']; ?></label></div></td>
                                                        <td class="list_item_account"><div><label class="label-table"><?php echo $h['category']; ?></label></div></td>
                                                        <td class="list_item_account"><div class='media'>
                                                            <div>
                                                                <label class="label-table">
                                                                    <span class="price"><?php echo $amout; ?>€</strong>
                                                                </label>
                                                            </div>
                                                            </div>
                                                        </td>
                                                        <td class="list_item_account"><div><label class="label-table"><?php echo $date_end.' à '.$date_end_h; ?></label></div></td>

                                                        <td class="list_item_account"><div>
                                                            <a class="btn btn-info" data-toggle="modal" href="../product/<?php echo $h['id']; ?>">Détails</a>
                                                            </div>
                                                        </td>
                                                <?php
                                                } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                        } else {
                             ?>
                                <div>
                                    <h2>Vous n'avez pas de produit en vente</h2>
                                </div>
                            <?php
                        }
                    ?>
                </div>
                
                
                
                
                
                
                
            </div>
        </div>
    </div>
</div>
