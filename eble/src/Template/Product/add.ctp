
<!-- echo $this->Form->create($product);
echo $this->Form->hidden('user_id', array('default' => $this->request->session()->read('Auth.User.id') ));
echo $this->Form->input('title');
echo $this->Form->input('description');
echo $this->Form->input('category');
echo $this->Form->input('date_start');
echo $this->Form->input('date_end');
echo $this->Form->input('price_start');
echo $this->Form->input('price_instant');
echo $this->Form->hidden('is_over', array('default' => 0));
echo $this->Form->submit();
echo $this->Form->end(); -->
<?php
	use Cake\I18n\Date;
?>

<div class="" id="content">
	<script type="text/javascript">
	    $(document).ready(function(e){
	        $('.search_category .dropdown-menu').find('a').click(function(e)
	        {
	          e.preventDefault();
	          var category = $(this).attr("href").replace("#","");
	          var innerButton = $(this).text();
	         $('input.form-control.input_addProduct#category').val(category);
	     	});


	     	 $(function () {
                $('#date_end').datetimepicker({
                    locale: 'fr',
                    format: 'YYYY-MM-DD HH:mm:ss'
                });
            });

	     	$('#formAddProduct').submit(function() {
	     		
			    $("#category").prop('disabled', false);

			});
	    });
	  </script>

	<div class="col-md-offset-4 col-md-4 col-md-offset-4" style="margin-top: 50px;">
	    <form role="form" action="" method="post" class="registration-form" id="formAddProduct">
	    	<?php echo $this->Form->hidden('user_id', array('default' => $this->request->session()->read('Auth.User.id') )); ?>
	    	<!--?php echo $this->Form->hidden('is_over', array('default' => 0)); ?-->
	    	<?php echo $this->Form->hidden('date_start', array('default' => new Date(null, new DateTimeZone('Europe/Paris')))); ?>
	    	<div class="form-group">
	    		<label class="sr-only" for="title">Titre</label>
	        	<input type="text" name="title" placeholder="Titre" class="form-first-name form-control" id="title">
	        </div>
	        <div class="input-group" style="margin-bottom: 15px;">
                <input readonly="readonly" id="category" type="text" name="category" value="Animalerie" placeholder="Animalerie" class="form-control input_addProduct">
                <div class="input-group-btn search_category">
                    <button type="button" class="btn btn-default dropdown-toggle dropdown_addProduct" data-toggle="dropdown">
                        <span id="category_dropdown"></span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                       	  <li class="selected"><a href="#Animalerie">Animalerie</a></li>
	                      <li><a href="#Bricolage">Bricolage</a></li>
	                      <li><a href="#Chaussures">Chaussures</a></li>
	                      <li><a href="#DVD, cinéma">DVD, cinéma</a></li>
	                      <li><a href="#Électroménager">Électroménager</a></li>
	                      <li><a href="#Informatique">Informatique</a></li>
	                      <li><a href="#Livres">Livres</a></li>
	                      <li><a href="#Musique">Musique</a></li>
	                      <li><a href="#Salle de bain">Salle de bain</a></li>
	                      <li><a href="#Sport">Sport</a></li>
                    </ul>
                </div>
            </div>
            <div class="form-group">
                 <input type="text" name="date_end" class="form-control" placeholder="2016-07-07 19:50:00" id='date_end'>
            </div>
	         <div class="form-group">
	        	<label class="sr-only" for="price_start">Prix de départ</label>
	        	<input type="decimal" name="price_start" placeholder="prix de départ" class="form-number form-control" id="price_start">
	        </div>
	        <div class="form-group">
	        	<label class="sr-only" for="price_instant">Prix d'achat immédiat</label>
	        	<input type="decimal" name="price_instant" placeholder="prix d'achat immédiat (supérieur au prix de départ)" class="form-email form-control" id="price_instant">
	        </div>
	        <div class="form-group">
	        	<label class="sr-only" for="description">Descritpion</label>
	        	<textarea name="description" placeholder="Description ..." class="form-about-yourself form-control" id="description"></textarea>
	        </div>
	        <button type="submit" class="btn" style="color:white;">Ajouter</button>
	    </form>
	</div>
	<div><?php echo $this->Flash->render() ?></div>
</div>

