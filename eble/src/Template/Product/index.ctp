<?php
use Cake\I18n\Time;
use Cake\Routing\Router;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*$bidController = new App\Controller\BidController();
$bidController->getLastBid($product['id']);
$_SESSION['product_id'] = $product['id'];*/

$product = $product->toArray();
if (count($product) == 1)
{
    $bids = $bids->toArray();

    $product =  $product[0];

    $date_start = explode(" ", date("d/m/Y H:i", strtotime($product['date_start'])));
    $date_end = explode(" ", date("d/m/Y H:i", strtotime($product['date_end'])));
?>
<h3><?= $product['title'] ?></h3>
<hr>
<div class="col-md-6">
    <div class="detail">
        <div><label>Categorie : </label> <?= $product['category'] ?></div>
        <div><label>Description : </label> <?= $product['description'] ?></div>
        <div><label>Prix de départ : </label> <?= $product['price_start'] ?> €</div>
        <div><label>Date de début : </label> <?= $date_start[0] ?> à <?= $date_start[1] ?></div>
        <div><label>Date de fin : </label> <?= $date_end[0] ?> à <?= $date_end[1] ?></div>
        <div><label>Dernière enchère : </label> <?= $lastbid ?> €</div>
    </div>
    <?php

        if (count($bids) > 1)
        {
        ?>
            <a href="<?= Router::url('/product/productbids/'.$product['id']); ?>"><?= count($bids); ?> enchères</a>
    <?php
        } else {
    ?>
            <a href="<?= Router::url('/product/productbids/'.$product['id']); ?>"><?= count($bids); ?> enchère</a>
    <?php
        }
    ?>

</div>

<?php
    if ($product['date_end'] > Time::now())
    {
?>

<div class="col-md-6" id="content">
        <?php
        if ($lastbid < $product['price_instant'] && $product['user_id'] != $user_id)
        {
        ?>
            <h4>Pressés d'obtenir ce produit ?<br>Achetez-le tout de suite à <?= $product['price_instant']; ?>€ !</h4>
            <a href="<?= Router::url('/bid/instantBuy/'.$product['id']); ?>" class="btn btn-info">Achat instantané</a>
        <?php
        }
        if ($product['user_id'] != $user_id)
        {
            ?>    <div class="form form-box"> <?php

        echo $this->Form->create(null, [
            'url' => ['controller' => 'Bid', 'action' => 'add/'.$product['id']]
        ]);
        echo $this->Form->hidden('user_id', array('default' => $this->request->session()->read('Auth.User.id')));
        echo $this->Form->hidden('product_id', array('default' => $product['id']));
        echo $this->Form->hidden('date', array('default' => date('Y-m-d H:i:s')));
        echo $this->Form->input('amount');
        echo $this->Form->hidden('is_instant', array('default' => 0));
        echo $this->Form->button('Enchérir');
        echo $this->Form->end();
        }
        ?>
    </div>
</div>
<br/>
<br/>
<br/>
<?php
    }
}
else
{
    foreach ($product as $prd)
    {
            echo $prd['title']."<br/>";
            echo $prd['description']."<br/>";
            echo $prd['category']."<br/>";
            echo $prd['price_start']."<br/>";
            echo $prd['price_instant']."<br/>";
            echo $prd['date_start']."<br/>";
            echo $prd['date_end']."<br/>";
            echo $prd['user_id']."<br/>";
    }
}
?>
<?= $this->Flash->render() ?>