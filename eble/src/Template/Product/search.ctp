<?php
    if (count($results) > 0)
    {
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-container">
                <table class="table table-filter">
                 <thead>
                    <tr>
                        <th class="resource-name">Produit</th>
                        <th class="resource-name">Catégorie</th>
                        <th class="resource-name">Prix (achat instantanée)</th>
                        <th></th>
                    </tr>
                </thead>
                    <tbody>
                        <?php
                            foreach ($results as $product)
                            {
                            ?>
                                 <tr data-status='En cours'>
                                    <td class="list_item_account"><div><label class="label-table"><?php echo $product['title']; ?></label></div></td>
                                    <td class="list_item_account"><div><label class="label-table"><?php echo $product['category']; ?></label></div></td>
                                    <td class="list_item_account"><div class='media'>
                                        <div>
                                            <label class="label-table">
                                             <span class="price"><?php echo $product['price_instant']; ?>€</strong>
                                            </label>
                                        </div>
                                        </div>
                                    </td>
                                    <td class="list_item_account"><div>
                                        <a class="btn btn-info" data-toggle="modal" href="../product/<?php echo $product['id']; ?>">Détails</a>
                                        </div>
                                    </td>
                            <?php
                            }
                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
    } else {
?>
            <div>
                <h2>Aucun résultat n'a été trouvé</h2>
            </div>
<?php
    }
?>