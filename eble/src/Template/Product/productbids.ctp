<?php
use Cake\Routing\Router;

?>
<div class="container">

    <h2>Historique des enchères</h2>
    <br/>
    <div class="pull-left">
        <a href="<?= Router::url('/product/'.$product_id); ?>"><span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>Retourner à la description de l'objet</a>
    </div>
    <div class="col-md-8 center-col">
        <div class="panel panel-bids">
            <?php
            $date_end = explode(' ', date("d/m/Y H:i", strtotime($product_end)));
            echo count($users_bids)." enchères - ";
            echo "Fin le ".$date_end[0].' à '.$date_end[1];
            ?>
            <table class="table">
                <thead>
                    <tr>
                        <th>Enchérisseurs</th>
                        <th>Montants des enchères</th>
                        <th>Dates des enchères</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php
                        foreach ($users_bids as $bid)
                        {
                            $date = explode(' ', date("d/m/Y H:i", strtotime($bid['date'])));
                            ?>
                            <td><?= $bid['firstname'] ?></td>
                            <td><?= $bid['amount'] ?> €</td>
                            <td><?= $date[0] ?> à <?= $date[1] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>