<div class="col-md-offset-2 col-md-8 col-md-offset-2" id="content">
	<?= $this->Flash->render() ?>
	
    <div class="">
		<div class="container-fluid">
			<div class="list-group">
		        <?php

		        	if (count($products) > 0)
					{
						foreach ($products as $res)
						{ 
                                                    
							$date = date("d/m/Y H:i", strtotime($res['date_end']));
							$date_end = explode(" ", $date)[0];
							$date_end_h = explode(" ", $date)[1];
						?>
			                <div class="item col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-group-item">
                                            <a class="bid-item" href="product/<?php echo $res['id'] ?>">
				                <div class="thumbnail">
				                    <div class="caption">
				                        <h3 class="group inner list-group-item-heading"><?php echo $res['title']; ?></h3>
				                        <p class="max-text-with"><?php echo $res['category']; ?></p>
				                        <hr />
				                        <div class="group inner list-group-item-text italic max-text-with">
				                        Fin le <strong><?php echo $date_end; ?></strong> à <strong><?php echo $date_end_h; ?></strong> </div>
				                        <div class="group inner list-group-item-text">Prix de départ: <span class="price"><?php echo $res['price_start']; ?>€</span></div>
                                                        <?php 
                                                        if ($res['max_amount'] != null)
                                                        {
                                                        ?>
				                        <div class="group inner list-group-item-text">Enchère actuelle: <span class="price"><?php echo $res['max_amount']; ?>€</span></div>
                                                        <?php
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                        <div class="group inner list-group-item-text">Aucune enchère à l'heure actuelle</div>

                                                        <?php
                                                        }
                                                        ?>

				                        <a class="btn btn-warning" data-toggle="modal" href="product/<?php echo $res['id'] ?>">Détails</a>
				                    </div>
				                </div>
                                            </a>

				            </div>
		                <?php
		                }
					} else {
						 ?>
							<div class="main_title text_wrap">
								<h1>Il n'y a aucun produit pour le moment</h1>
								<h2>Soyez le premier !</h2>
								<a class="upload_button btn btn-primary" href="?upload">
								<span class="fa fa-product-hunt"></span>Vendre un produit</a>
							</div>
						<?php
					}
		        ?>
	        </div>
		</div>
	</div>
</div>