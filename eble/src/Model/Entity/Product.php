<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\TUser $t_user
 * @property string $title
 * @property string $description
 * @property string $category
 * @property \Cake\I18n\Time $date_start
 * @property \Cake\I18n\Time $date_end
 * @property float $price_start
 * @property float $price_instant
 * @property string $is_over
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
    
    public function check()
    {
        return ($this->date_start < $this->date_end);
    }
}
